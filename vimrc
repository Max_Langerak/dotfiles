filetype on
filetype indent on
filetype plugin on
syntax on

"Set the colors to 256, better with tmux
set t_Co=256

set number
set autoindent
set smartindent
set cindent

set tabstop=3
set shiftwidth=3
set expandtab

set ttyfast

set incsearch
set hlsearch

set showmatch

set cursorline
set cul "highlight current line
hi CursorLine term=none cterm=none ctermbg=8

colorscheme Monokai 

"wrap long lines
set wrap
"set nowrap

"set the right filetype
set ff=unix

"put ruler on steroids
set rulerformat=%30(%=\:b%n%y%m%r%w\ %l,%c%V\ %P%)

"Starting rainbow parentheses
au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces

"try making vim popup behave like an IDE popup
set completeopt=longest,menuone
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

"Make the cursor move as expected with wrapped lines
inoremap <Down> <C-o>gj
inoremap <Up> <C-o>gk

"open URL in browser
function! Browser()
   let line = getline(".")
   let line = matchstr (line, "http[^ ]*")
   exec "!firefox ".line
endfunction

"Change the mapleader to , because it is easier to type
let mapleader=","

"Change the <Leader><Leader> from easymotion back to just <Leader> (Is faster)
let g:EasyMotion_leader_key='<Leader>'

"Toggle NERDTree using F3
nmap <silent> <F3> :NERDTreeToggle<CR>

if has('statusline')
   set laststatus=2

   set statusline=%<%f\
   set statusline+=%w%h%m%r
   if has('fugitive')
      set statusline+=%{fugitive#statusline()}
   endif
   set statusline+=\ [%{&ff}/%Y]
   set statusline+=\ [%{getcwd()}]
   set statusline+=%=%-14.(%l,%c%V%)\ %p%%
endif

"easier moving in tabs and windows
map <C-J> <C-W>j<C-W>_
map <C-K> <C-W>k<C-W>_
map <C-L> <C-W>l<C-W>_
map <C-H> <C-W>h<C-W>_

"Change the working directory to that of current file
cmap cwd lcd %:p:h
cmap cd. lcd %:p:h
lcd %:p:h

"markdown folding disabled
let g:vim_markdown_folding_disabled=1
